===============
openEO Core API
===============

This is the python based reference implementation of the openEO core API.

Description
===========

This is the reference implementation of the openEO core API definition. The basis of this implementation is
the Python Flask-RESTful framework.
This example server exposes the openEO core API definition as swagger.json file.

The idea behind this Flask-RESTful implementation of the openEO Core API is:

    * The whole openEO Core API description is based on a ready to run Python implementation.
    * Other projects can use this implementation to expose their backends via the openEO Core API
      while providing a valid swagger.json definition of all implemented services.
      Endpoints that are not implemented by the backend can provide the default responses.
    * The actual implementation of the API and the test suite assures meaningful API definitions.
    * All examples of the openEO Core API are exposed as valid responses to requests.

Attention
---------

This implementation exposes only the examples of the openEO Core API as responses. It does not
implement backend functionality.

The available test suite cheks if the examples were delivered correctly by the Python Flask-RESTful framework.
It does not test any backend functionality.

Installation
============

It is preferred to run the openEO reference implementation in a virtual python environment.

Create directory that contains the code and the virtual environment and switch the environment:

    .. code-block:: bash

        mkdir openEO
        cd openEO
        virtualenv -p python3.5 venv
        source venv/bin/activate

Clone the official repository and install the required Python packages into the virtual environment:

    .. code-block:: bash

        git clone https://bitbucket.org/huhabla/openeo_core.git openeo_core
        cd openeo_core
        pip install -r requirements.txt
        python setup.py install

Run the openEO Core API test suite:

    .. code-block::

        python setup.py test

Run the test server:

    .. code-block:: bash

        python -m openeo_core.main

Get the swagger.json API description using curl:

    .. code-block:: bash

        curl -X GET http://localhost:5000/api/v0/swagger.json
