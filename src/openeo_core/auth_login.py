# -*- coding: utf-8 -*-
from flask import make_response, jsonify
from flask_restful import Resource
from flask_restful_swagger_2 import swagger

__author__ = "Sören Gebbert"
__copyright__ = "Copyright 2018, Sören Gebbert"
__maintainer__ = "Soeren Gebbert"
__email__ = "soerengebbert@googlemail.com"

AUTH_LOGIN_EXAMPLE = {"text/plain; charset=utf-8": "b34ba2bdf9ac9ee1"}

AUTH_LOGIN_DOC = {
    "summary": "Check whether a user is registered at the back-end.",
    "description": "This request simply checks whether the provided HTTP `Authorization` header refers to a "
                   "valid user at the back-end and returns his/her internal user ID. It is not needed to call "
                   "login before sending any other API request, which will also expect the HTTP `Authorization` "
                   "header if needed. Back-ends that do not require authentication such as a local file-based "
                   "implementation may always return a generic user ID such as `me`.",
    "tags": ["Authentication"],
    "produces": ["text/plain; charset=utf-8"],
    "responses": {
        "200": {
            "description": "User ID of the user that refers to the provided HTTP `Authorization` header. ",
            "schema": {
                "type": "string"
            },
            "examples": AUTH_LOGIN_EXAMPLE
        },
        "403": {
            "description": "Login failed"
        }
    }
}


class AuthLogin(Resource):
    @swagger.doc(AUTH_LOGIN_DOC)
    def get(self):
        return make_response(jsonify(AUTH_LOGIN_EXAMPLE), 200)
