# -*- coding: utf-8 -*-
from flask import make_response, jsonify
from flask_restful import abort, Resource
from flask_restful_swagger_2 import swagger
from openeo_core.definitions import Job

__author__ = "Sören Gebbert"
__copyright__ = "Copyright 2018, Sören Gebbert"
__maintainer__ = "Soeren Gebbert"
__email__ = "soerengebbert@googlemail.com"

GET_DOWNLOAD_EXAMPLE = {"description": "A valid response"}

GET_DOWNLOAD_DOC = {
    "summary": "Endpoint to download job results in the specified format.",
    "description": "This request will ask the back-end to fetch job result data in the specified format.  "
                   "Input arguments depend on the format requested, e.g., for a WMTS service it takes "
                   "zoom level, tile indexes, visuallization parameters, and time if needed.",
    "tags": ["Data Download"],
    "produces": [],
    "parameters": [
        {
            "name": "format",
            "in": "path",
            "type": "string",
            "description": "string specifying the data format to deliver",
            "required": True,
            "enum": ["nc", "json", "wcs", "wmts", "tms", "tif", "png", "jpeg"]
        },
        {
            "name": "job_id",
            "in": "path",
            "type": "string",
            "description": "job identifier string",
            "required": True
        }
    ],
    "responses": {
        "200": {"description": "A valid response"},
        "401": {"$ref": "#/responses/auth_required"},
        "403": {"$ref": "#/responses/access_denied"},
        "404": {"description": "Job with specified identifier is not available."},
        "406": {"description": "The server is not capable to deliver the requested format."},
        "501": {"$ref": "#/responses/not_implemented"},
        "503": {"$ref": "#/responses/unavailable"}
    }
}


class Download(Resource):
    @swagger.doc(GET_DOWNLOAD_DOC)
    def get(self, format, job_id):
        return make_response(jsonify(GET_DOWNLOAD_EXAMPLE), 200)
