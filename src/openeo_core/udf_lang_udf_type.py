# -*- coding: utf-8 -*-
from flask import make_response, jsonify
from flask_restful import abort, Resource
from flask_restful_swagger_2 import swagger
from openeo_core.definitions import UDFDescription

__author__ = "Sören Gebbert"
__copyright__ = "Copyright 2018, Sören Gebbert"
__maintainer__ = "Soeren Gebbert"
__email__ = "soerengebbert@googlemail.com"

GET_UDF_TYPE_EXAMPLE = UDFDescription.example

GET_UDF_TYPE_DOC = {
    "summary": "Returns the process description of UDF schemas, which offer different possibilities how "
               "user-defined scripts can be applied to the data.",
    "tags": ["UDF"],
    "parameters": [
        {
            "name": "lang",
            "in": "path",
            "description": "Language identifier such as `R`",
            "type": "string",
            "enum": ["python", "R"],
            "required": True
        },
        {
            "name": "udf_type",
            "in": "path",
            "type": "string",
            "description": "The UDF types define how UDFs can be exposed to the data, how they can be parallelized, "
                           "and how the result schema should be structured.",
            "enum": ["apply_pixel", "apply_scene", "reduce_time", "reduce_space", "window_time", "window_space",
                     "window_spacetime", "aggregate_time", "aggregate_space", "aggregate_spacetime"],
            "required": True
        }
    ],
    "responses": {
        "200": {
            "description": "Process description",
            "schema": UDFDescription,
            "examples": {"application/json": GET_UDF_TYPE_EXAMPLE}
        },
        "401": {"$ref": "#/responses/auth_required"},
        "403": {"$ref": "#/responses/access_denied"},
        "404": {"description": "UDF type with specified identifier is not available"},
        "501": {"description": "This API feature, language or UDF type is not supported by the back-end."},
        "503": {"$ref": "#/responses/unavailable"}
    }
}


class UdfType(Resource):
    @swagger.doc(GET_UDF_TYPE_DOC)
    def get(self, lang, udf_type):
        return make_response(jsonify(GET_UDF_TYPE_EXAMPLE), 200)
