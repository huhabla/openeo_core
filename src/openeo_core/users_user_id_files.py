# -*- coding: utf-8 -*-
from flask import make_response, jsonify
from flask_restful import abort, Resource
from flask_restful_swagger_2 import swagger
from openeo_core.definitions import UserId, FileDescription

__author__ = "Sören Gebbert"
__copyright__ = "Copyright 2018, Sören Gebbert"
__maintainer__ = "Soeren Gebbert"
__email__ = "soerengebbert@googlemail.com"

GET_USERS_FILES_EXAMPLE = [
    {
        "name": "test.txt",
        "size": 182,
        "modified": "2015-10-20T17:22:10Z"
    },
    {
        "name": "test.tif",
        "size": 183142,
        "modified": "2017-01-01T09:36:18Z"
    },
    {
        "name": "Sentinel2/S2A_MSIL1C_20170819T082011_N0205_R121_T34KGD_20170819T084427.zip",
        "size": 4183353142,
        "modified": "2018-01-03T10:55:29Z"
    }
]

GET_USERS_FILES_DOC = {
    "summary": "List user-uploaded files",
    "description": "This service lists all user-uploaded files that are stored at the back-end.",
    "tags": ["User Content"],
    "parameters": [UserId],
    "responses": {
        "200": {
            "description": "Flattened file tree with path relative to the user's root directory and some basic "
                           "properties such as the file size and the timestamp of the last modification. "
                           "All properties except the name are optional.",
            "schema": {
                "type": "array",
                "items": FileDescription
            },
            "examples": {"application/json": GET_USERS_FILES_EXAMPLE}
        },
        "401": {"$ref": "#/responses/auth_required"},
        "403": {"$ref": "#/responses/access_denied"},
        "404": {"description": "User with specified user id is not available."},
        "501": {"$ref": "#/responses/not_implemented"},
        "503": {"$ref": "#/responses/unavailable"}
    }
}


class UsersFiles(Resource):
    @swagger.doc(GET_USERS_FILES_DOC)
    def get(self, user_id):
        return make_response(jsonify(GET_USERS_FILES_EXAMPLE), 200)
