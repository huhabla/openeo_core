# -*- coding: utf-8 -*-
from flask import make_response, jsonify
from flask_restful import abort, Resource
from flask_restful_swagger_2 import swagger
from openeo_core.definitions import Job

__author__ = "Sören Gebbert"
__copyright__ = "Copyright 2018, Sören Gebbert"
__maintainer__ = "Soeren Gebbert"
__email__ = "soerengebbert@googlemail.com"

GET_JOBS_ID_CANCEL_EXAMPLE = {"description": "The job has been successfully canceled."}

GET_JOBS_ID_CANCEL_DOC = {
    "summary": "Cancels any back-end computations of a job",
    "description": "This request cancels all related computations at the back-end. "
                   "It will stop generating additional costs for processing. Results of batch jobs might "
                   "still be accessible whereas lazy jobs will generally not respond to s following "
                   "download calls.  ",
    "tags": [
        "Job Management"
    ],
    "parameters": [
        {
            "name": "job_id",
            "in": "path",
            "type": "string",
            "description": "job identifier string",
            "required": True
        }
    ],
    "responses": {
        "200": {"description": "The job has been successfully canceled."},
        "401": {"$ref": "#/responses/auth_required"},
        "403": {"$ref": "#/responses/access_denied"},
        "404": {"description": "Job with specified identifier is not available"},
        "501": {"$ref": "#/responses/not_implemented"},
        "503": {"$ref": "#/responses/unavailable"}
    }
}


class JobsJobIdCancel(Resource):
    @swagger.doc(GET_JOBS_ID_CANCEL_DOC)
    def get(self, job_id):
        # TODO: Maybe using POST would be better?
        return make_response(jsonify(GET_JOBS_ID_CANCEL_EXAMPLE), 200)
