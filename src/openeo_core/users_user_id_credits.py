# -*- coding: utf-8 -*-
from flask import make_response, jsonify
from flask_restful import abort, Resource
from flask_restful_swagger_2 import swagger
from openeo_core.definitions import UserId

__author__ = "Sören Gebbert"
__copyright__ = "Copyright 2018, Sören Gebbert"
__maintainer__ = "Soeren Gebbert"
__email__ = "soerengebbert@googlemail.com"

GET_USERS_CREDITS_EXAMPLE = "210"

GET_USERS_CREDITS_DOC = {
    "summary": "Returns available user credits",
    "description": "For back-ends that involve accounting, this service will return the currently "
                   "available credits. Other back-ends may simply return `Infinity`.",
    "tags": ["User Content"],
    "parameters": [UserId],
    "produces": ["text/plain; charset=utf-8"],
    "responses": {
        "200": {
            "description": "Available credits",
            "schema": {
                "description": "Available credits",
                "type": "number"
            },
            "examples": {
                "text/plain": GET_USERS_CREDITS_EXAMPLE
            }
        },
        "401": {"$ref": "#/responses/auth_required"},
        "403": {"$ref": "#/responses/access_denied"},
        "404": {"description": "User with specified identifier is not available."},
        "501": {"$ref": "#/responses/not_implemented"},
        "503": {"$ref": "#/responses/unavailable"}
    }
}


class UsersCredits(Resource):
    @swagger.doc(GET_USERS_CREDITS_DOC)
    def get(self, user_id):
        return make_response(GET_USERS_CREDITS_EXAMPLE, 200)
