# -*- coding: utf-8 -*-
from flask import make_response, jsonify
from flask_restful import abort, Resource
from flask_restful_swagger_2 import swagger
from openeo_core.definitions import Job
from openeo_core.definitions import UserId, FileDescription

__author__ = "Sören Gebbert"
__copyright__ = "Copyright 2018, Sören Gebbert"
__maintainer__ = "Soeren Gebbert"
__email__ = "soerengebbert@googlemail.com"

GET_USERS_FILES_PATH_EXAMPLE = ""

GET_USERS_FILES_PATH_DOC = {
    "summary": "Download a user file",
    "description": "This service downloads a user files identified by its path relative "
                   "to the user's root directory.",
    "tags": ["User Content"],
    "produces": ["application/octet-stream"],
    "parameters": [UserId,
                   {
                       "name": "path",
                       "in": "path",
                       "type": "string",
                       "description": "path relative to the user's root directory, must be URL encoded",
                       "required": True
                   }
                   ],
    "responses": {
        "200": {
            "description": "file from user storage",
            "schema": {"type": "file"}
        },
        "401": {"$ref": "#/responses/auth_required"},
        "403": {"$ref": "#/responses/access_denied"},
        "404": {"description": "File or user with specified identifier is not available"},
        "501": {"$ref": "#/responses/not_implemented"},
        "503": {"$ref": "#/responses/unavailable"}
    }
}

PUT_USERS_FILES_PATH_EXAMPLE = {"description": "The file upload has been successful."}

PUT_USERS_FILES_PATH_DOC = {
    "summary": "Upload a new or update an existing user file",
    "description": "This service uploads a new or updates an existing file at a given path.",
    "tags": ["User Content"],
    "consumes": ["multipart/form-data"],
    "parameters": [UserId,
                   {
                       "name": "path",
                       "in": "path",
                       "type": "string",
                       "description": "path relative to the user's root directory, must be URL encoded",
                       "required": True
                   },
                   {
                       "name": "file",
                       "in": "formData",
                       "type": "file",
                       "description": "file to upload",
                       "required": True
                   }
                   ],
    "responses": {
        "200": {"description": "The file upload has been successful."},
        "401": {"$ref": "#/responses/auth_required"},
        "403": {"$ref": "#/responses/access_denied"},
        "404": {"description": "User with specified identifier is not available."},
        "422": {"description": "File is rejected."},
        "423": {"description": "The file that is being accessed is locked."},
        "501": {"$ref": "#/responses/not_implemented"},
        "503": {"$ref": "#/responses/unavailable"},
        "507": {"description": "User exceeded his storage limit."}
    }
}

DELETE_USERS_FILES_PATH_EXAMPLE = {"description": "The file has been successfully deleted at the back-end."}

DELETE_USERS_FILES_PATH_DOC = {
    "summary": "Deletes a user file",
    "description": "This service deletes an existing user-uploaded file specified by its path.",
    "tags": ["User Content"],
    "parameters": [UserId,
                   {
                       "name": "path",
                       "in": "path",
                       "type": "string",
                       "description": "path relative to the user's root directory, must be URL encoded",
                       "required": True
                   }
                   ],
    "responses": {
        "200": {"description": "The file has been successfully deleted at the back-end."},
        "401": {"$ref": "#/responses/auth_required"},
        "403": {"$ref": "#/responses/access_denied"},
        "404": {"description": "File or user with specified identifier is not available."},
        "423": {"description": "The file that is being accessed is locked."},
        "501": {"$ref": "#/responses/not_implemented"},
        "503": {"$ref": "#/responses/unavailable"}
    }
}


class UsersFilesPath(Resource):
    @swagger.doc(GET_USERS_FILES_PATH_DOC)
    def get(self, user_id, path):
        return make_response(jsonify(GET_USERS_FILES_PATH_EXAMPLE), 200)

    @swagger.doc(PUT_USERS_FILES_PATH_DOC)
    def put(self, user_id, path):
        return make_response(jsonify(PUT_USERS_FILES_PATH_EXAMPLE), 200)

    @swagger.doc(DELETE_USERS_FILES_PATH_DOC)
    def delete(self, user_id, path):
        return make_response(jsonify(DELETE_USERS_FILES_PATH_EXAMPLE), 200)
