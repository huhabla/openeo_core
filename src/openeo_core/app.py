# -*- coding: utf-8 -*-
from flask import Flask
from flask_cors import CORS
from flask_restful_swagger_2 import Api

__author__ = "Sören Gebbert"
__copyright__ = "Copyright 2018, Sören Gebbert"
__maintainer__ = "Soeren Gebbert"
__email__ = "soerengebbert@googlemail.com"

flask_app = Flask(__name__)
CORS(flask_app)

flask_api = Api(flask_app,
                api_version='0.1pre-alpha',
                api_spec_url='/api/v0/swagger',
                title="OpenEO Core API",
                description="The OpenEO API specification for interoperable cloud-based processing of large "
                            "Earth observation datasets. **This early draft version is incomplete and intended "
                            "for working on a prototype and a proof of concept.** Things that are currently missing "
                            "particularly include: \n "
                            "* Authentication and authorization with OAuth 2.0, \n "
                            "* how results of computations can be downloaded, \n "
                            "* how data is streamed into UDFs and how the output of UDFs is returned, \n "
                            "* how services are organized as microservices, \n * how payments can be handled, \n "
                            "* and how OpenSearch is interfaced",
                schemes=['http', 'https'],
                consumes=['application/gml+xml', 'application/json'])

flask_api._swagger_object["securityDefinitions"] = {"basicAuth": {"type": "basic"}}
flask_api._swagger_object["security"] = [{"basicAuth": []}]
flask_api._swagger_object["tags"] = [
    {
        "name": "API Information",
        "description": "General services about the API implementation at a specific back-end"
    },
    {
        "name": "EO Data Discovery",
        "description": "Discovery of Earth observation datasets that are available at the back-end"
    },
    {
        "name": "Process Discovery",
        "description": "Discovery of processes that are available at the back-end"
    },
    {
        "name": "UDF",
        "description": "Interfacing end executing user-defined functions at the back-end."
    },
    {
        "name": "User Content",
        "description": "Services working with user content"
    },
    {
        "name": "Authentication",
        "description": "Authentication of users"
    },
    {
        "name": "Job Management",
        "description": "Organization of processing jobs that have been submitted to the back-end"
    },
    {
        "name": "Data Download",
        "description": "Services for downloading different types of job results"
    }
]

flask_api._swagger_object["responses"] = {
    "auth_required": {
        "description": "The back-end requires clients to authenticate in order to process this request."
    },
    "not_implemented": {
        "description": "This API feature is not supported by the back-end."
    },
    "access_denied": {
        "description": "Authorization failed, access to the requested resource has been denied."
    },
    "unavailable": {
        "description": "The service is currently unavailable."
    }
}
