# -*- coding: utf-8 -*-
from flask import make_response
from flask_restful import Resource
from flask_restful_swagger_2 import swagger

__author__ = "Sören Gebbert"
__copyright__ = "Copyright 2018, Sören Gebbert"
__maintainer__ = "Soeren Gebbert"
__email__ = "soerengebbert@googlemail.com"

GET_DATA_OPENSEARCH_EXAMPLE = """<?xml version=\"1.0\" encoding=\"UTF-8\"?> "
                                        "<feed xmlns=\"http://www.w3.org/2005/Atom\" \n  "
                                        "xmlns:opensearch=\"http://a9.com/-/spec/opensearch/1.1/\">\n  "
                                        "<title>Supported EO datasets by Example, Inc.</title>\n  "
                                        "<updated>2018-01-18T10:30:00Z</updated>\n  <author>\n    "
                                        "<name>Example, Inc.</name>\n  </author> \n  "
                                        "<id>urn:uuid:60a76c80-d399-11d9-b93C-0003939e0af6</id>\n  "
                                        "<link rel=\"self\" href=\"http://www.exmaple.com/api/1.0/data/opensearch\"/>\n"
                                        "  <link rel=\"search\" type=\"application/opensearchdescription+xml\" "
                                        "href=\"http://example.com/api/1.0/data/opensearch/description.xml\"/>\n  "
                                        "<opensearch:totalResults>3</opensearch:totalResults>\n  "
                                        "<opensearch:Query role=\"request\" startPage=\"1\" />\n  "
                                        "<entry>\n    <title>MOD09Q1</title>\n    <link rel=\"alternate\" "
                                        "href=\"http://www.exmaple.com/api/1.0/data/MOD09Q1\"/>\n    "
                                        "<id>urn:uuid:1223c695-cfb8-4ebb-aaaa-80da344efa6a</id>\n    "
                                        "<updated>2017-09-18T10:30:00Z</updated>\n    "
                                        "<rights type=\"text\">U.S. Geological Survey (USGS), DOI: "
                                        "10.5067/MODIS/MOD09Q1.006</rights>\n    "
                                        "<summary type=\"text\">MODIS/Terra Surface Reflectance 8-Day L3 Global 250m "
                                        "SIN Grid V00 </summary>\n  </entry>\n  <entry>\n    "
                                        "<title>SENTINEL2-1C</title>\n    <link rel=\"alternate\" "
                                        "href=\"http://www.exmaple.com/api/1.0/data/SENTINEL2-1C\"/>\n    "
                                        "<id>urn:uuid:1224c695-cfc8-4ebb-bbbb-90da344efa6b</id>\n    "
                                        "<updated>2017-12-01T01:00:00Z</updated>\n    "
                                        "<rights type=\"text\">European Space Agency (ESA)</rights>\n    "
                                        "<summary type=\"text\">Sentinel 2 Level-1C: Top-of-atmosphere reflectances "
                                        "in cartographic geometry</summary>\n  </entry>\n  <entry>\n    "
                                        "<title>LandsatETM+</title>\n    <link rel=\"alternate\" "
                                        "href=\"http://www.exmaple.com/api/1.0/data/LandsatETM%2B\"/>\n    "
                                        "<id>urn:uuid:1225c695-cfd8-4ebb-cccc-70da344efa6c</id>\n    "
                                        "<updated>2015-01-10T22:45:12Z</updated>\n    "
                                        "<rights type=\"text\">U.S. Geological Survey (USGS)</rights>\n    "
                                        "<summary type=\"text\">Landsat Enhanced Thematic Mapper Plus "
                                        "(ETM+)</summary>\n  </entry>\n</feed>"""

GET_DATA_OPENSEARCH_DOC = {
    "summary": "OpenSearch endpoint to receive standardized data search results.",
    "description": "This service offers more complex search functionality and returns "
                   "results in an OpenSearch compliant Atom XML format.",
    "tags": ["EO Data Discovery"],
    "produces": ["application/atom+xml"],
    "parameters": [
        {
            "in": "query",
            "name": "q",
            "type": "string",
            "description": "string expression to search available datasets",
            "required": False
        },
        {
            "in": "query",
            "name": "start",
            "type": "integer",
            "description": "page start value",
            "required": False
        },
        {
            "in": "query",
            "name": "rows",
            "type": "integer",
            "description": "page size value",
            "required": False
        }
    ],
    "responses": {
        "200": {
            "description": "An array of EO datasets including their unique identifiers and some basic metadata.",
            "examples": {"application/atom+xml": GET_DATA_OPENSEARCH_EXAMPLE}
        },
        "401": {"$ref": "#/responses/auth_required"},
        "501": {"$ref": "#/responses/not_implemented"},
        "503": {"$ref": "#/responses/unavailable"}
    }
}


class DataOpenSearch(Resource):
    @swagger.doc(GET_DATA_OPENSEARCH_DOC)
    def get(self):
        return make_response(GET_DATA_OPENSEARCH_EXAMPLE, 200)
