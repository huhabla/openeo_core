# -*- coding: utf-8 -*-
from flask import Flask, make_response, jsonify
from flask_restful import reqparse, abort, Api, Resource
from flask_restful_swagger_2 import Api, swagger
from openeo_core.definitions import DataSetListEntry

__author__ = "Sören Gebbert"
__copyright__ = "Copyright 2018, Sören Gebbert"
__maintainer__ = "Soeren Gebbert"
__email__ = "soerengebbert@googlemail.com"

GET_DATA_EXAMPLE = [
    {
        "product_id": "MOD09Q1",
        "description": " MODIS/Terra Surface Reflectance 8-Day L3 Global 250m SIN Grid V006",
        "source": "U.S. Geological Survey (USGS), DOI: 10.5067/MODIS/MOD09Q1.006"
    },
    {
        "product_id": "SENTINEL2-1C",
        "description": "Sentinel 2 Level-1C: Top-of-atmosphere reflectances in cartographic geometry",
        "source": "European Space Agency (ESA)"
    },
    {
        "product_id": "LandsatETM+",
        "description": "Landsat Enhanced Thematic Mapper Plus (ETM+)",
        "source": "U.S. Geological Survey (USGS)"
    }
]

GET_DATA_DOC = {
    "summary": "Returns basic information about EO datasets that are available at the back-end and "
               "offers simple search by time, space, and product name.",
    "description": "Requests will ask the back-end for available data and will return an array of available datasets "
                   "with very basic information such as their unique identifiers. Results can be filtered by space, "
                   "time, and product name with very simple search expressions.",
    "tags": ["EO Data Discovery"],
    "parameters": [
        {
            "in": "query",
            "name": "qname",
            "type": "string",
            "description": "string expression to search available datasets by name",
            "required": False
        },
        {
            "in": "query",
            "name": "qgeom",
            "type": "string",
            "description": "WKT polygon to search for available datasets that spatially intersect with the polygon",
            "required": False
        },
        {
            "in": "query",
            "name": "qstartdate",
            "type": "string",
            "description": "ISO 8601 date/time string to find datasets with any data acquired after the given date/time",
            "required": False
        },
        {
            "in": "query",
            "name": "qenddate",
            "type": "string",
            "description": "ISO 8601 date/time string to find datasets with any data acquired before the given date/time",
            "required": False
        }
    ],
    "responses": {
        "200": {
            "description": "An array of EO datasets including their unique identifiers and some basic metadata.",
            "schema": {"type": "array",
                       "items": DataSetListEntry
                       },
            "examples": {"application/json": GET_DATA_EXAMPLE}
        },
        "401": {"$ref": "#/responses/auth_required"},
        "501": {"$ref": "#/responses/not_implemented"},
        "503": {"$ref": "#/responses/unavailable"}
    }
}


class Data(Resource):
    @swagger.doc(GET_DATA_DOC)
    def get(self):
        return make_response(jsonify(GET_DATA_EXAMPLE), 200)
