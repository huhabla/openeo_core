# -*- coding: utf-8 -*-
from flask import make_response, jsonify
from flask_restful import abort, Resource
from flask_restful_swagger_2 import swagger
from openeo_core.definitions import Job

__author__ = "Sören Gebbert"
__copyright__ = "Copyright 2018, Sören Gebbert"
__maintainer__ = "Soeren Gebbert"
__email__ = "soerengebbert@googlemail.com"

GET_JOBS_ID_DOC = {
    "summary": "Returns information about a submitted job",
    "description": "Returns detailed information about a submitted job including its "
                   "current status and the underlying task",
    "tags": ["Job Management"],
    "parameters": [
        {
            "name": "job_id",
            "in": "path",
            "type": "string",
            "description": "job identifier string",
            "required": True
        }
    ],
    "responses": {
        "200": {"description": "JSON object with job information.", "schema": Job},
        "401": {"$ref": "#/responses/auth_required"},
        "403": {"$ref": "#/responses/access_denied"},
        "404": {"description": "Job with specified identifier is not available"},
        "501": {"$ref": "#/responses/not_implemented"},
        "503": {"$ref": "#/responses/unavailable"}
    }
}

DELETE_JOBS_ID_EXAMPLE = {
    "job_id": "42d5k3nd92mk49dmj294md",
    "status": "scheduled",
    "process_graph": {
        "process_id": "slope",
        "args": {
            "dem": {
                "process_id": "filter_bbox",
                "args": {
                    "imagery": "/data/srtm90m",
                    "srs": "EPSG:4326",
                    "left": 6.301,
                    "right": 7.232,
                    "top": 53.87,
                    "bottom": 50.223
                }
            }
        }
    },
    "submitted": "2017-01-01T09:32:12Z",
    "user_id": "ab32e5f3a2bc2847s2"
}

DELETE_JOBS_ID_DOC = {
    "summary": "Deletes a submitted job",
    "description": "Deleting a job  will cancel execution at the back-end regardless of its status. "
                   "For finished jobs, this will also delete resulting data.",
    "tags": ["Job Management"],
    "parameters": [
        {
            "name": "job_id",
            "in": "path",
            "type": "string",
            "description": "job identifier string",
            "required": True
        }
    ],
    "responses": {
        "200": {
            "description": "JSON object with job information.",
            "examples": {"application/json": DELETE_JOBS_ID_EXAMPLE}
        },
        "401": {"$ref": "#/responses/auth_required"},
        "403": {"$ref": "#/responses/access_denied"},
        "404": {"description": "Job with specified identifier is not available"},
        "501": {"$ref": "#/responses/not_implemented"},
        "503": {"$ref": "#/responses/unavailable"}
    }
}


class JobsJobId(Resource):
    @swagger.doc(GET_JOBS_ID_DOC)
    def get(self, job_id):
        return make_response(jsonify(Job.example), 200)

    @swagger.doc(DELETE_JOBS_ID_DOC)
    def delete(self, job_id):
        return make_response(jsonify(DELETE_JOBS_ID_EXAMPLE), 200)
