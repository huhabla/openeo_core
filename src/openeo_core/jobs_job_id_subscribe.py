# -*- coding: utf-8 -*-
from flask import make_response, jsonify
from flask_restful import abort, Resource
from flask_restful_swagger_2 import swagger
from openeo_core.definitions import Job

__author__ = "Sören Gebbert"
__copyright__ = "Copyright 2018, Sören Gebbert"
__maintainer__ = "Soeren Gebbert"
__email__ = "soerengebbert@googlemail.com"

GET_JOBS_ID_SUB_EXAMPLE = "THE PROTOCOL FOR COMMUNICATION OF JOB UPDATES IS TO BE DEFINED."

GET_JOBS_ID_SUB_DOC = {
    "summary": "Subscribes to job execution updates that are communicated over WebSockets",
    "description": "THE PROTOCOL FOR COMMUNICATION OF JOB UPDATES IS TO BE DEFINED.",
    "schemes": ["wss"],
    "tags": ["Job Management"],
    "produces": [],
    "parameters": [
        {
            "name": "job_id",
            "in": "path",
            "type": "string",
            "description": "job identifier string",
            "required": True
        },
        {
            "name": "Upgrade",
            "in": "header",
            "type": "string",
            "enum": ["websocket"],
            "description": "WebSocket handshake request header",
            "required": True
        },
        {
            "name": "Connection",
            "in": "header",
            "type": "string",
            "enum": [
                "Upgrade"
            ],
            "description": "WebSocket handshake request header",
            "required": True
        },
        {
            "name": "Sec-WebSocket-Key",
            "in": "header",
            "type": "string",
            "description": "WebSocket handshake request header",
            "required": True
        },
        {
            "name": "Sec-WebSocket-Protocol",
            "in": "header",
            "type": "string",
            "enum": [
                "job_subscribe"
            ],
            "description": "WebSocket handshake request header",
            "required": True
        },
        {
            "name": "Sec-WebSocket-Version",
            "in": "header",
            "type": "number",
            "enum": [
                13
            ],
            "description": "WebSocket handshake request header",
            "required": True
        }
    ],
    "responses": {
        "101": {"description": "Successful subscription to job updates returns in a protocol change "
                               "to a web socket connection."},
        "401": {"$ref": "#/responses/auth_required"},
        "403": {"$ref": "#/responses/access_denied"},
        "404": {"description": "Job with specified identifier is not available"},
        "501": {"$ref": "#/responses/not_implemented"},
        "503": {"$ref": "#/responses/unavailable"}
    }
}


class JobsJobIdSubscribe(Resource):
    @swagger.doc(GET_JOBS_ID_SUB_DOC)
    def get(self, job_id):
        # TODO: Maybe using POST would be better?
        return make_response(GET_JOBS_ID_SUB_EXAMPLE, 200)
