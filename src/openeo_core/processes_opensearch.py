# -*- coding: utf-8 -*-
from flask import make_response, jsonify
from flask_restful import abort, Resource
from flask_restful_swagger_2 import swagger

__author__ = "Sören Gebbert"
__copyright__ = "Copyright 2018, Sören Gebbert"
__maintainer__ = "Soeren Gebbert"
__email__ = "soerengebbert@googlemail.com"

GET_PROCESSES_OPENSEARCH_EXAMPLE = """"<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<feed xmlns=\"http://www.w3.org/2005/Atom\" \n  xmlns:opensearch=\"http://a9.com/-/spec/opensearch/1.1/\">\n
<title>Supported EO processes by Example, Inc.</title>\n  <updated>2018-01-18T10:30:00Z</updated>\n  <author>\n
<name>Example, Inc.</name>\n  </author> \n  <id>urn:uuid:60a76c80-d399-11d9-b93C-0003939e0af6</id>\n
<link rel=\"self\" href=\"http://www.exmaple.com/api/1.0/processes/opensearch\"/>\n  <link rel=\"search\"
type=\"application/opensearchdescription+xml\"
href=\"http://example.com/api/1.0/processes/opensearch/description.xml\"/>\n
<opensearch:totalResults>2</opensearch:totalResults>\n  <opensearch:Query role=\"request\" startPage=\"1\" />\n
<entry>\n    <title>NDVI</title>\n
<link rel=\"alternate\" href=\"http://www.exmaple.com/api/1.0/processes/NDVI\"/>\n
<id>urn:uuid:1223c695-cfb8-4ebb-aaaa-80da344efa6a</id>\n    <updated>2018-01-18T10:30:00Z</updated>\n
<summary type=\"text\">Computes the normalized difference vegetation index (NDVI) for all pixels of the
input dataset.</summary>\n  </entry>\n  <entry>\n    <title>median_time</title>\n    <link rel=\"alternate\"
href=\"http://www.exmaple.com/api/1.0/data/median_time\"/>\n
<id>urn:uuid:1224c695-cfc8-4ebb-bbbb-90da344efa6b</id>\n    <updated>2018-01-18T10:30:00Z</updated>\n
<summary type=\"text\">Applies median aggregation to pixel time series for all bands of the input dataset.</summary>\n
</entry>\n</feed>"
"""

GET_PROCESSES_OPENSEARCH_DOC = {
    "summary": "OpenSearch endpoint to request standardized process search results.",
    "description": "This service offers more complex search functionality and returns "
                   "results in an OpenSearch compliant Atom XML format.",
    "tags": ["Process Discovery"],
    "parameters": [
        {
            "name": "q",
            "in": "query",
            "type": "string",
            "description": "string expression to search available processes",
            "required": False
        },
        {
            "name": "start",
            "in": "query",
            "type": "integer",
            "description": "page start value",
            "required": False
        },
        {
            "name": "rows",
            "in": "query",
            "type": "integer",
            "description": "page size value",
            "required": False
        }
    ],
    "responses": {
        "200": {
            "description": "OpenSearch response",
            "examples": GET_PROCESSES_OPENSEARCH_EXAMPLE
        },
        "401": {"$ref": "#/responses/auth_required"},
        "501": {"$ref": "#/responses/not_implemented"},
        "503": {"$ref": "#/responses/unavailable"}
    }
}


class ProcessesOpenSearch(Resource):
    @swagger.doc(GET_PROCESSES_OPENSEARCH_DOC)
    def get(self, ):
        return make_response(jsonify(GET_PROCESSES_OPENSEARCH_EXAMPLE), 200)
