# -*- coding: utf-8 -*-
from flask import make_response, jsonify
from flask_restful import abort, Resource
from flask_restful_swagger_2 import swagger
from openeo_core.definitions import Job
from openeo_core.definitions import UserId

__author__ = "Sören Gebbert"
__copyright__ = "Copyright 2018, Sören Gebbert"
__maintainer__ = "Soeren Gebbert"
__email__ = "soerengebbert@googlemail.com"

GET_USERS_JOBS_EXAMPLE = [Job.example, Job.example]

GET_USERS_JOBS_DOC = {
    "summary": "List all jobs that have been submitted by the user",
    "description": "Requests to this service will list all jobs submitted by a user with given id.",
    "tags": ["Job Management", "User Content"],
    "parameters": [UserId],
    "responses": {
        "200": {
            "description": "Array of job descriptions",
            "schema": {
                "description": "Array of job descriptions",
                "type": "array",
                "items": Job
            }
        },
        "401": {"$ref": "#/responses/auth_required"},
        "403": {"$ref": "#/responses/access_denied"},
        "404": {"description": "User with specified identifier is not available."},
        "501": {"$ref": "#/responses/not_implemented"},
        "503": {"$ref": "#/responses/unavailable"}
    }
}


class UsersJobs(Resource):
    @swagger.doc(GET_USERS_JOBS_DOC)
    def get(self, user_id):
        return make_response(jsonify(GET_USERS_JOBS_EXAMPLE), 200)
